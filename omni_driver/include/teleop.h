#include <ros/ros.h>

#include <sensor_msgs/JointState.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <omni_msgs/OmniButtonEvent.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Float64.h>

#include <eigen3/Eigen/Dense>

#include <tf/transform_listener.h>


class omniTeleop
{
private:
    // ROS
    ros::NodeHandlePtr node;

    // Command gains
    std::vector<double> joint_command_gain;
    std::vector<double> twist_command_gain;
    std::vector<double> force_feedback_gain;
    std::vector<double> joint_torque_feedback_gain;

    // Topics remapping
    std::string slave_joint_states_topic;
    std::string slave_force_feedback_topic;
    std::string joint_command_topic;
    std::string twist_command_topic;
    std::string gripper_command_topic;
    std::string control_mode_topic;
    std::string master_joint_command_topic;

    // Other parameters
    std::vector<double> slave_effort_offset;
    bool teleop_master;
    int slave_manipulator;
    int control_mode_n;
    int control_mode_i=0;
    std::vector<double> control_mode_vector;

    // Publishers and Subscribers
    ros::Subscriber sub_joint_states;
    ros::Subscriber sub_twist;
    ros::Subscriber sub_button_state;
    // if teleop=true
    ros::Subscriber sub_joint_states_slave;
    ros::Subscriber sub_force_feedback;
    ros::Publisher pub_force_feedback;
    ros::Publisher pub_torque_feedback;
    ros::Publisher pub_joint_command;
    ros::Publisher pub_twist_command;
    ros::Publisher pub_control_mode;
    ros::Publisher pub_gripper_command;
    ros::Subscriber sub_control_mode;
    // if teleop=false
    ros::Subscriber sub_joint_command_master;
    ros::Publisher pub_joint_torque;
    ros::Publisher pub_enable_control;

    // Callbacks
    void jointStateCallback(const sensor_msgs::JointState::ConstPtr& msg);
    void buttonStateCallback(const omni_msgs::OmniButtonEvent::ConstPtr& msg);
    void twistCallback(const geometry_msgs::TwistStamped::ConstPtr& twistStamped);
    // if teleop=true
    void jointStateSlaveCallback(const sensor_msgs::JointState::ConstPtr& joint_states);
    void forceFeedbackCallback(const geometry_msgs::WrenchStamped::ConstPtr& wrenchStamped);
    void controlModeCallback(const std_msgs::Int8::ConstPtr& msg);
    // if teleop=false
    void jointCommandMasterCallback(const sensor_msgs::JointState::ConstPtr& msg);

    // Messages
    sensor_msgs::JointState joint_states;
    sensor_msgs::JointState joint_states_slave;
    sensor_msgs::JointState joint_command;
    geometry_msgs::TwistStamped twist;
    geometry_msgs::TwistStamped twist_command;
    geometry_msgs::WrenchStamped wrench;
    geometry_msgs::WrenchStamped force_feedback;
    geometry_msgs::Vector3 joint_torque;
    geometry_msgs::Vector3 joint_torque_feedback;
    omni_msgs::OmniButtonEvent button_state;
    std_msgs::Bool enable_control;
    std_msgs::Int8 control_mode_msg;
    std_msgs::Float64 gripper_command;

    // Variables
    tf::TransformListener tfListener;
    unsigned int slave_dof;
    std::vector<double> joint_master_ref;
    std::vector<double> joint_slave_ref;

    // Functions
    void setParameters();
    void resizeMessages();
    void calculateJointCommand();
    void calculateTwistCommand();
    void calculateForceFeedbackCommand();
    void calculateTorqueFeedbackCommand();
    void calculateGripperCommand();
    void calculateJointTorque();
    void transformTwist();
    void transformTwist(geometry_msgs::TwistStamped& twist_msg, const std::string& target_frame);

    inline void checkSize(std::vector<double>& vector, unsigned int n){
        if (vector.size() != 1 && vector.size() != n) {
            ROS_WARN_STREAM("Gain should be represented as a scalar or a " << n << " elements vector. Setting gain to 1");
            vector.resize(n);
            std::fill(vector.begin(),vector.end(), 1.0);
        }
        if (vector.size() == 1) {
            double gain = vector.at(0);
            vector.resize(n);
            std::fill(vector.begin(),vector.end(), gain);
        }
    }

public:
    int control_mode;
    enum ControlMode {Everything = 0, JointPos, JointVel, Twist, TwistLin, TwistAng};

    omniTeleop();

    void publishTopics();
};

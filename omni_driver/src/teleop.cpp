// Master Slave Operation. Should only depend on topics already published and subscribed by omnibase.cpp

#include <../include/teleop.h>

omniTeleop::omniTeleop()
{
    // ROS
    node = ros::NodeHandlePtr(new ros::NodeHandle(""));

    setParameters();

    // Publishers and Subscribers
    if (teleop_master) {
        sub_joint_states = node->subscribe("joint_states", 1, &omniTeleop::jointStateCallback, this);
        sub_twist = node->subscribe("twist", 1, &omniTeleop::twistCallback, this);
        sub_button_state = node->subscribe("button_state", 1, &omniTeleop::buttonStateCallback, this);
        
        sub_force_feedback = node->subscribe(slave_force_feedback_topic, 1, &omniTeleop::forceFeedbackCallback, this);
        sub_joint_states_slave = node->subscribe(slave_joint_states_topic, 1, &omniTeleop::jointStateSlaveCallback, this);
        sub_control_mode = node->subscribe<std_msgs::Int8>("control_mode_sub_useless", 1, &omniTeleop::controlModeCallback, this); // this was an attempt to cycle through control modes in this node, but it should not be implemented. Double check before erasing

        pub_joint_command = node->advertise<sensor_msgs::JointState>(joint_command_topic,10);
        pub_twist_command = node->advertise<geometry_msgs::TwistStamped>(twist_command_topic,10);
        pub_control_mode = node->advertise<std_msgs::Int8>(control_mode_topic,10);
        pub_gripper_command = node->advertise<std_msgs::Float64>(gripper_command_topic, 10);

        pub_force_feedback = node->advertise<geometry_msgs::WrenchStamped>("in/force",10);
        pub_torque_feedback = node->advertise<geometry_msgs::Vector3>("in/joint_torque",10);
    } else {
        sub_button_state = node->subscribe("button_state", 1, &omniTeleop::buttonStateCallback, this);
        sub_joint_command_master = node->subscribe(master_joint_command_topic, 1, &omniTeleop::jointCommandMasterCallback, this);

        pub_joint_torque = node->advertise<geometry_msgs::Vector3>("in/joint_torque",10);
        pub_enable_control = node->advertise<std_msgs::Bool>("in/enable_control",10);
    }

    resizeMessages();
}

void omniTeleop::setParameters() {
    ros::param::param<bool>("~teleop_master", teleop_master, true);

    if (teleop_master) {
        ros::param::param<int>("~slave_manipulator", slave_manipulator, 0);

        switch (slave_manipulator) {
        case 2:
            slave_joint_states_topic = "/doris/manipulator_controller/joint_states";
            slave_force_feedback_topic = "/doris/manipulator_controller/force";
            joint_command_topic = "/doris/manipulator_controller/joint_cmd";
            twist_command_topic = "/doris/manipulator_controller/twist_cmd";
            control_mode_topic = "/doris/manipulator_controller/remote_enable";
            slave_dof = 4;
            break;
        case 3:
            slave_joint_states_topic = "/kinova/base_feedback/joint_state";
            slave_force_feedback_topic = "slave_force_feedback";  // dummy, kinova does not have end-effector force sensor
            joint_command_topic = "/kinova_remap/joint_command";
            twist_command_topic = "/kinova_remap/twist_command";
            gripper_command_topic = "/kinova_remap/gripper_command";
            control_mode_topic = "/kinova_remap/control_mode";
            slave_dof = 7;
            break;
        default:
            slave_joint_states_topic = "slave_joint_states";
            slave_force_feedback_topic = "slave_force_feedback";
            joint_command_topic = "joint_command";
            twist_command_topic = "twist_command";
            control_mode_topic = "control_mode";
            slave_dof = 6;
            break;
        }

        ros::param::get("~twist_command_gain", twist_command_gain);
        checkSize(twist_command_gain, 6);

        ros::param::get("~joint_command_gain", joint_command_gain);
        checkSize(joint_command_gain, slave_dof);

        ros::param::get("~force_feedback_gain", force_feedback_gain);
        checkSize(force_feedback_gain, 6);

        ros::param::get("~joint_torque_feedback_gain", joint_torque_feedback_gain);
        checkSize(joint_torque_feedback_gain, 3);

        ros::param::get("~control_mode_vector", control_mode_vector);
        control_mode_n=control_mode_vector.size();

        slave_effort_offset = {0.0, 0.0, 0.0};

        control_mode = Everything;
        ROS_INFO("PHANTOM TELEOP: Publishing every command message");

    } else {
        master_joint_command_topic = "master_joint_command";
        slave_dof = 3;
    }
}

void omniTeleop::resizeMessages() {
    joint_states.position.resize(6);
    joint_states.velocity.resize(6);
    joint_states.effort.resize(6);

    joint_command.position.resize(slave_dof);
    joint_command.velocity.resize(slave_dof);
    joint_command.effort.resize(slave_dof);

    joint_states_slave.position.resize(slave_dof);
    joint_states_slave.velocity.resize(slave_dof);
    joint_states_slave.effort.resize(slave_dof);
}

void omniTeleop::controlModeCallback(const std_msgs::Int8::ConstPtr& msg) {
    control_mode = msg->data;
    switch (control_mode) {
        case Everything:
            ROS_INFO("PHANTOM TELEOP: Publishing every command message");
            break;
        case JointPos:
            ROS_INFO("PHANTOM TELEOP: Joint Position control");
            break;
        case JointVel:
            ROS_INFO("PHANTOM TELEOP: Joint Velocity control");
            break;
        case Twist:
            ROS_INFO("PHANTOM TELEOP: Cartesian Velocity control");
            break;
        case TwistLin:
            ROS_INFO("PHANTOM TELEOP: Linear Cartesian Velocity control");
            break;
        case TwistAng:
            ROS_INFO("PHANTOM TELEOP: Angular Cartesian Velocity control");
            break;
        default:
            ROS_WARN("PHANTOM TELEOP: Invalid control mode!");
            break;
    }
}

void omniTeleop::jointStateCallback(const sensor_msgs::JointState::ConstPtr& msg) {
    for(int i = 0; i < 6; i++){
        joint_states.position.at(i) = msg->position.at(i);
        joint_states.velocity.at(i) = msg->velocity[i];
        joint_states.effort.at(i) = msg->effort[i];
    }
}

void omniTeleop::twistCallback(const geometry_msgs::TwistStamped::ConstPtr& twistStamped) {
    twist = *twistStamped;
}

void omniTeleop::buttonStateCallback(const omni_msgs::OmniButtonEvent::ConstPtr& msg) {    
    button_state.grey_button = msg->grey_button;
    button_state.grey_button_clicked = msg->grey_button_clicked;
    button_state.white_button = msg->white_button;
    button_state.white_button_clicked = msg->white_button_clicked;   

    if (!teleop_master) {
        if (button_state.grey_button_clicked) {
            enable_control.data = true;
            pub_enable_control.publish(enable_control);
        }
        if (button_state.white_button_clicked)
        {
            enable_control.data = false;
            pub_enable_control.publish(enable_control);
        }
    }
    else {  // teleop_master == true
        if (button_state.white_button_clicked) // Change slave mode control with each click
        {
            control_mode_msg.data=control_mode_vector[control_mode_i];
            pub_control_mode.publish(control_mode_msg);
            control_mode_i++;
            if (control_mode_i == control_mode_n)  control_mode_i=0;
        }
    }
}

void omniTeleop::jointStateSlaveCallback(const sensor_msgs::JointState::ConstPtr& joint_states) {
    joint_states_slave.position = joint_states->position;
    joint_states_slave.velocity = joint_states->velocity;
    joint_states_slave.effort = joint_states->effort;
}

void omniTeleop::forceFeedbackCallback(const geometry_msgs::WrenchStamped::ConstPtr& wrenchStamped) {
    wrench = *wrenchStamped;
}

void omniTeleop::calculateJointCommand() {
    joint_command.header.stamp = ros::Time::now();
    if (button_state.grey_button) {   // || button_state.white_button) {
            switch (slave_manipulator) {
                case 2:
                    joint_command.position.at(0) = joint_command_gain.at(0)*(joint_states.position.at(0) - joint_master_ref.at(0)) + joint_slave_ref.at(0);
                    joint_command.position.at(1) = joint_command_gain.at(1)*(joint_states.position.at(1) - joint_master_ref.at(1)) + joint_slave_ref.at(1);
                    joint_command.position.at(2) = joint_command_gain.at(2)*(joint_states.position.at(2) - joint_master_ref.at(2)) + joint_slave_ref.at(2);
                    joint_command.position.at(3) = joint_command_gain.at(3)*(joint_states.position.at(4) - joint_master_ref.at(4)) + joint_slave_ref.at(3);
                    
                    joint_command.velocity.at(0) = joint_command_gain.at(0)*joint_states.velocity.at(0);
                    joint_command.velocity.at(1) = joint_command_gain.at(1)*joint_states.velocity.at(1);
                    joint_command.velocity.at(2) = joint_command_gain.at(2)*joint_states.velocity.at(2);
                    joint_command.velocity.at(3) = joint_command_gain.at(3)*joint_states.velocity.at(4);
                    break;
                case 3:
                    joint_command.position.at(0) = joint_command_gain.at(0)*(joint_states.position.at(0) - joint_master_ref.at(0)) + joint_slave_ref.at(0);
                    joint_command.position.at(1) = joint_command_gain.at(1)*(joint_states.position.at(1) - joint_master_ref.at(1)) + joint_slave_ref.at(1);
                    joint_command.position.at(2) = (joint_slave_ref.at(2));
                    joint_command.position.at(3) = joint_command_gain.at(3)*(joint_states.position.at(2) - joint_master_ref.at(2)) + joint_slave_ref.at(3);
                    joint_command.position.at(4) = joint_command_gain.at(4)*(joint_states.position.at(3) - joint_master_ref.at(3)) + joint_slave_ref.at(4);
                    joint_command.position.at(5) = joint_command_gain.at(5)*(joint_states.position.at(4) - joint_master_ref.at(4)) + joint_slave_ref.at(5);
                    joint_command.position.at(6) = joint_command_gain.at(6)*(joint_states.position.at(5) - joint_master_ref.at(5)) + joint_slave_ref.at(6);
                    
                    joint_command.velocity.at(0) = joint_command_gain.at(0)*joint_states.velocity.at(0);
                    joint_command.velocity.at(1) = joint_command_gain.at(1)*joint_states.velocity.at(1);
                    joint_command.velocity.at(2) = 0.0;
                    joint_command.velocity.at(3) = joint_command_gain.at(3)*joint_states.velocity.at(2);
                    joint_command.velocity.at(4) = joint_command_gain.at(4)*joint_states.velocity.at(3);
                    joint_command.velocity.at(5) = joint_command_gain.at(5)*joint_states.velocity.at(4);
                    joint_command.velocity.at(6) = joint_command_gain.at(6)*joint_states.velocity.at(5);
                    break;
                default:
                    for (int i = 0; i < 6; i++) {
                        joint_command.position.at(i) = joint_command_gain.at(i)*(joint_states.position.at(i) - joint_master_ref.at(i)) + joint_slave_ref.at(i);
                        joint_command.velocity.at(i) = joint_command_gain.at(i)*joint_states.velocity.at(i);
                    }
                    break;
            }            
    } else {
        joint_master_ref = joint_states.position;
        joint_slave_ref = joint_states_slave.position;

        for (unsigned int i = 0; i < slave_dof; ++i) {
            joint_command.position.at(i) = joint_states_slave.position.at(i);
            joint_command.velocity.at(i) = 0.0;
        }
    }
}

void omniTeleop::calculateTwistCommand()
{
    twist_command.header.stamp = ros::Time::now();
    twist_command.header.frame_id = twist.header.frame_id;

    if (button_state.grey_button) {   // || button_state.white_button) {
        switch (slave_manipulator) {
            case 2:
                twist_command.twist.linear.x = 1000*twist_command_gain.at(0)*twist.twist.linear.y; // tetis twist in mm/s
                twist_command.twist.linear.y = -1000*twist_command_gain.at(1)*twist.twist.linear.x;
                twist_command.twist.linear.z = 1000*twist_command_gain.at(2)*twist.twist.linear.z;
                twist_command.twist.angular.x = twist_command_gain.at(3)*twist.twist.angular.y;
                twist_command.twist.angular.y = -twist_command_gain.at(4)*twist.twist.angular.x;
                twist_command.twist.angular.z = twist_command_gain.at(5)*twist.twist.angular.z;
                break;

            case 3:
                twist_command.twist.linear.x = twist_command_gain.at(0)*twist.twist.linear.y;
                twist_command.twist.linear.y = -twist_command_gain.at(1)*twist.twist.linear.x;
                twist_command.twist.linear.z = twist_command_gain.at(2)*twist.twist.linear.z;
                twist_command.twist.angular.x = twist_command_gain.at(3)*twist.twist.angular.y;
                twist_command.twist.angular.y = -twist_command_gain.at(4)*twist.twist.angular.x;
                twist_command.twist.angular.z = twist_command_gain.at(5)*twist.twist.angular.z;
                break;

            default:
                twist_command.twist.linear.x = twist_command_gain.at(0)*twist.twist.linear.x;
                twist_command.twist.linear.y = twist_command_gain.at(1)*twist.twist.linear.y;
                twist_command.twist.linear.z = twist_command_gain.at(2)*twist.twist.linear.z;
                twist_command.twist.angular.x = twist_command_gain.at(3)*twist.twist.angular.x;
                twist_command.twist.angular.y = twist_command_gain.at(4)*twist.twist.angular.y;
                twist_command.twist.angular.z = twist_command_gain.at(5)*twist.twist.angular.z;
                break;
        }
    } else {
        twist_command.twist.linear.x = 0.0;
        twist_command.twist.linear.y = 0.0;
        twist_command.twist.linear.z = 0.0;
        twist_command.twist.angular.x = 0.0;
        twist_command.twist.angular.y = 0.0;
        twist_command.twist.angular.z = 0.0;
    }
    //twist_command.twist.linear = rotation*twist_command.twist.linear;
}

void omniTeleop::transformTwist(geometry_msgs::TwistStamped& twist_msg, const std::string& target_frame) {

    geometry_msgs::Vector3Stamped twist_linear;
    geometry_msgs::Vector3Stamped twist_angular;
    
    static geometry_msgs::Vector3Stamped twist_linear_rotated;
    static geometry_msgs::Vector3Stamped twist_angular_rotated;
    
    twist_linear.header = twist_msg.header;
    twist_linear.vector = twist_msg.twist.linear;
    
    twist_angular.header = twist_msg.header;
    twist_angular.vector = twist_msg.twist.angular;

    ROS_INFO_STREAM("twist_linear.header.frame_id = " << twist_linear.header.frame_id);
    ROS_INFO_STREAM("target_frame = " << target_frame);

    ros::Time now = ros::Time::now();
    if (tfListener.waitForTransform(target_frame, "base", now, ros::Duration(3.0))) { // now -> ros::Time(0) ? // latest available
        tfListener.transformVector(target_frame,twist_linear,twist_linear_rotated);
        tfListener.transformVector(target_frame,twist_angular,twist_angular_rotated);
        // tfListener.transformVector(target_frame,now,twist_linear,"base",twist_linear_rotated);
    } else {
        // Maybe twist should not be sent?
    }

    twist_msg.header.frame_id = target_frame;
    twist_msg.twist.linear = twist_linear_rotated.vector;
    twist_msg.twist.angular = twist_angular_rotated.vector;
}

void omniTeleop::calculateForceFeedbackCommand() {
    force_feedback.header = wrench.header;        
    if (button_state.grey_button) {   // || button_state.white_button) {
        force_feedback.wrench.force.x = 0.001*force_feedback_gain.at(0)*wrench.wrench.force.x;  // From tetis/doris in mN
        force_feedback.wrench.force.y = 0.001*force_feedback_gain.at(1)*wrench.wrench.force.y;
        force_feedback.wrench.force.z = 0.001*force_feedback_gain.at(2)*wrench.wrench.force.z;
        force_feedback.wrench.torque.x = 0.001*force_feedback_gain.at(3)*wrench.wrench.torque.x;
        force_feedback.wrench.torque.y = 0.001*force_feedback_gain.at(4)*wrench.wrench.torque.y;
        force_feedback.wrench.torque.z = 0.001*force_feedback_gain.at(5)*wrench.wrench.torque.z;
    } else {
        force_feedback.wrench.force.x = 0;
        force_feedback.wrench.force.y = 0;
        force_feedback.wrench.force.z = 0;
        force_feedback.wrench.torque.x = 0;
        force_feedback.wrench.torque.y = 0;
        force_feedback.wrench.torque.z = 0;
    }
}

void omniTeleop::calculateTorqueFeedbackCommand() {
    // joint_torque_feedback.header = joint_states_slave.header; // Change in/joint_torque msg from Vector3 to Vector3Stamped
    if (button_state.white_button) {   // button_state.white_button_clicked) {
        // RESET TORQUE / SET TORQUE OFFSET
        slave_effort_offset.at(0) = joint_states_slave.effort.at(0);
        slave_effort_offset.at(1) = joint_states_slave.effort.at(1);
        slave_effort_offset.at(2) = joint_states_slave.effort.at(3);
    }

    if (button_state.grey_button) {  //  || button_state.white_button) {
        joint_torque_feedback.x = joint_torque_feedback_gain.at(0)*(joint_states_slave.effort.at(0) - slave_effort_offset.at(0));
        joint_torque_feedback.y = joint_torque_feedback_gain.at(1)*(joint_states_slave.effort.at(1) - slave_effort_offset.at(1));
        joint_torque_feedback.z = joint_torque_feedback_gain.at(2)*(joint_states_slave.effort.at(3) - slave_effort_offset.at(2));
    } else {
        joint_torque_feedback.x = 0.0;
        joint_torque_feedback.y = 0.0;
        joint_torque_feedback.z = 0.0;
    }

    if (button_state.grey_button && button_state.white_button) {
        // BOTH BUTTONS CLICKED -> OFFSET = 0
    }
}

void omniTeleop::calculateGripperCommand(){
    double Te = 0.02; // sampling period (rate = 50 --> 1/50)
    double tau = 0.1; // time constant of the filter
    double Kv = 0.15;
    double command = joint_states.velocity.at(5);
    if (button_state.grey_button){
        if ((joint_states_slave.position.at(7) <= 0.01 && command > 0) || (joint_states_slave.position.at(7) >= 0.780 && command < 0)){
            gripper_command.data = 0;
        }
        else{
            gripper_command.data = Te/(tau + Te) * command * Kv + tau/(tau + Te) * gripper_command.data; //filtered command
        }
    }
    else {
        gripper_command.data = 0;
    }
}

// Phantom Slave
void omniTeleop::jointCommandMasterCallback(const sensor_msgs::JointState::ConstPtr& msg) {
    for (int i = 0; i < 3; i++) {
        joint_command.velocity.at(i) = msg->velocity.at(i);
    }
}

// Phantom Slave
void omniTeleop::calculateJointTorque() {
    joint_torque.x = joint_command.velocity.at(0);
    joint_torque.y = joint_command.velocity.at(1);
    joint_torque.z = joint_command.velocity.at(2);
}

void omniTeleop::publishTopics(){
    if (teleop_master) {
        // Reads joint_states, twist, button_state, slave_joint_states
        // Publishes joint_command and twist_command
        calculateJointCommand();

        // static std::string target_frame = "tip";
        // transformTwist(twist, target_frame); // Drops rate from 50hz to 20hz
        calculateTwistCommand();

        pub_joint_command.publish(joint_command);
        pub_twist_command.publish(twist_command);

        if (slave_manipulator == 2) {
            calculateForceFeedbackCommand();
            pub_force_feedback.publish(force_feedback); // it was outside if
        }
        if (slave_manipulator == 3) {
            calculateTorqueFeedbackCommand();
            pub_torque_feedback.publish(joint_torque_feedback);
            calculateGripperCommand();
            pub_gripper_command.publish(gripper_command);
        }
    } else {
        // Reads button_state, master_joint_command
        // Publishes torque and enable/disable control
        calculateJointTorque();

        pub_joint_torque.publish(joint_torque);
    }
    
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "teleop");

    omniTeleop omniTeleop;

    //ros::Duration(6.0).sleep(); // sleep due to tf delay

    ros::Rate rate(50);
    while (ros::ok()){

        ros::spinOnce();

        omniTeleop.publishTopics();

        rate.sleep();
    }

    return 0;
}

# About
This is a ROS metapackage for both Sensable Phantom Omni (IEEE1394 connection) 
and Geomagic Touch (Ethernet connection). Included in this metapackage is the
omni_driver package, which contains the *omni_driver* and *teleop* node. If you don't know or don't have ROS, you
should first go to http://www.ros.org/.

# Usage

## Firewire device

```sh 
$ roslaunch omni_driver firewire.launch
```
Additionally, you need to update the serial number on the omni.launch file so that it matches yours.
Go to omni_driver/launch/omni.launch and look for this line:
```sh
$ <param name="omni_type" type="string" value="$(arg type)" />
			<param name="omni_serial" type="string" value="11129400000" />
```
Then change the value to match your device's serial number.

## Ethernet device

```sh 
$ roslaunch omni_driver ethernet.launch
```

If you want to teleoperate one of them, start the Master as you would normally
then run the slave i.e:
```sh 
$ roslaunch omni_driver firewire.launch
$ roslaunch omni_driver ethernet_slave.launch
```

## Topics
The *omni_driver* node publishes the following topics:
- `[namespace]/joint_states` ([sensor_msgs/JointState](http://docs.ros.org/api/sensor_msgs/html/msg/JointState.html)) -- position, velocity and name of each joint.
- `[namespace]/pose` ([geometry_msgs/PoseStamped](http://docs.ros.org/api/geometry_msgs/html/msg/PoseStamped.html)) -- stylus tip pose with a timestamp.
- `[namespace]/twist` ([geometry_msgs/TwistStamped](http://docs.ros.org/api/geometry_msgs/html/msg/TwistStamped.html)) -- stylus tip linear and angular velocities with a timestamp.
- `[namespace]/button_state` (omni_msgs/OmniButtonEvent) -- state (pressed/released) of the device buttons.

Three topics are subscribed to interact with a robot manipulator (Tethis, Kinova, etc):
- `[namespace]/in/joint_torque` ([geometry_msgs/Vector3](http://docs.ros.org/api/geometry_msgs/html/msg/Vector3.html)) -- each component is read between [-1, 1] and controls a single motor. The (x, y, z) components command the joints 1, 2 and 3, respectively.
- `[namespace]/in/force` ([geometry_msgs/WrenchStamped](http://docs.ros.org/api/geometry_msgs/html/msg/WrenchStamped.html)) -- The wrench.force compoments command the force applied to stylus tip.
- `[namespace]/in/enable_control` ([std_msgs/Bool](http://docs.ros.org/api/std_msgs/html/msg/Bool.html)) -- enable/disable [true/false] the control (joint motors) to generate joint torque or stylus tip force.

The *teleop* node works in two modes: master or slave.  The *teleop* is launched if parameter `teleop:=true` in the launch file.

If `~teleop_master:=true` enables teleoperation  as master, then *teleop* node publishes the following topics:
- `[namespace]/joint_command` ([sensor_msgs/JointState](http://docs.ros.org/api/sensor_msgs/html/msg/JointState.html)) -- position and velocity of each joint, when grey button is being pressed. If grey button is not being pressed, the node publishes 0 to velocity field, and the current slave joint state to position field.
- `[namespace]/twist_command` ([geometry_msgs/TwistStamped](http://docs.ros.org/api/geometry_msgs/html/msg/TwistStamped.html)) -- twist, when grey button is being pressed. If grey button is not being pressed, the node publishes 0.
- `[namespace]/control_mode` ([std_msgs/Int8]) -- cycles through control mode vector (`~control_mode_vector`) when white button is pressed.
- `[namespace]/in/joint_torque` ([geometry_msgs/Vector3](http://docs.ros.org/api/geometry_msgs/html/msg/Vector3.html)) -- each component is read between [-1, 1] and controls a single motor. The (x, y, z) components command the joints 1, 2 and 3, respectively.
- `[namespace]/in/force` ([geometry_msgs/WrenchStamped](http://docs.ros.org/api/geometry_msgs/html/msg/WrenchStamped.html)) -- The wrench.force compoments command the force applied to stylus tip.

and it subscribes to the following topics:
- `[namespace]/button_state` (omni_msgs/OmniButtonEvent) -- state (pressed/released) of the device buttons in order to determine when to send commands.
- `[namespace]/joint_states` ([sensor_msgs/JointState](http://docs.ros.org/api/sensor_msgs/html/msg/JointState.html)) -- position, velocity of each joint, to calculate joint_command (velocity) and twist_command.
- `[namespace]/slave_force_feedback` ([geometry_msgs/WrenchStamped](http://docs.ros.org/api/geometry_msgs/html/msg/WrenchStamped.html)) -- force signal from slave manipulator end-effector. This topic is  published to `[namespace]/in/force` while grey button is being pressed.
- `[namespace]/slave_joint_states` ([sensor_msgs/JointState](http://docs.ros.org/api/sensor_msgs/html/msg/JointState.html)) -- position of each joint of slave manipulator, to calculate joint_command (position).
- `[namespace]/twist` ([geometry_msgs/TwistStamped](http://docs.ros.org/api/geometry_msgs/html/msg/TwistStamped.html)) -- position, velocity of each joint, to calculate joint_command (velocity) and twist_command.

If `~teleop_master:=false` enables teleoperation  as slave, then teleop node publishes to the following topics:
- `[namespace]/in/enable_control` ([std_msgs/Bool](http://docs.ros.org/api/std_msgs/html/msg/Bool.html)) -- Publishes message to enable/disable joint motors.
- `[namespace]/in/joint_torque` ([geometry_msgs/Vector3](http://docs.ros.org/api/geometry_msgs/html/msg/Vector3.html)) -- Publishes torque value for each joint motor.

and it subscribes to the following topics:
- `[namespace]/button_state` (omni_msgs/OmniButtonEvent) -- grey button to enable the control (joint motors), and white button to disable it.
- `[namespace]/master_joint_command_topic` ([sensor_msgs/JointState](http://docs.ros.org/api/sensor_msgs/html/msg/JointState.html)) -- the device reads the velocity values of master joints and attempts to mimic them.

<figure>
  <img src="/images/rosgraph.png" width="480" height="240">
  <figcaption>Nodes and Topics:  teleop_master:= true.</figcaption>
</figure>

## Parameters
As you should have notices by now, most of the topics can be prefixed. This is done by altering the parameter `~namespace` and/or running the node from a [group](http://wiki.ros.org/roslaunch/XML/group) with namespace inside a launch file. Below we list the available parameters:

| Parameter Name        |  Type  |      Options       | Default Value | Description |
| :--------------       | :----: | :----------------: | :-----------: | :---------- |
| `~namespace`          | string | any                | omniFirewire  | Prefix that is put before many of the topics names |
| `~omni_type`          | string | firewire, ethernet | firewire      | The communication type |
| `~omni_serial`        | string | any                |               | The serial number printed below the device |
| `~path_urdf`          | string | any                |               | Path to the URDF location |
| `~path_srdf`          | string | any                |               | Path to the SRDF location |
| `~tf_prefix`          | string | any                | omni          | Prefix that is put before the tf's |
| `~start_rviz`         | bool   | true_false         | true          | True for starting rviz at launch |
| `~teleop`             |  bool  | true, false        | true          | True for starting teleop node |
| `~teleop_master`      |  bool  | true, false        | true          | The teleoperation mode (true for master, false for slave) |
| `~slave_manipulator`  | int    | any                | 1             | Select slave manipulator (2 for tetis, 3 for kinova) |



# Dependencies

The following command will install the required dependencies to compile the program from source:
```sh
$ sudo apt-get update
$ sudo apt-get install build-essential libncurses5-dev freeglut3 dh-autoreconf \
    ros-xxxxx-ros-core ros-xxxxx-moveit ros-xxxxx-joy ros-xxxxx-joystick-drivers \
    libeigen3-dev libraw1394-dev libusb-dev libboost-all-dev 
```
This commands installs build-essential, libncurses 5, freeglut 3, dh-autoreconf, ROS-xxxxx, MoveIt! and joy drivers for ROS-Indigo, Eigen, Boost, IEEE1394 (FireWire) driver, and libusb-dev.

## Geomagic Touch Device Drivers and OpenHaptics
As I can't distribute Sensable's software, those you'll have to download directly
from them. Please note that to access the download section, you may have to make
an account on their forum.

- https://3dsystems.teamplatform.com/pages/102863?t=fptvcy2zbkcc

From there, you'll need both the OpenHaptics and the GTDD files. Just extract it
and run the install script.


# Final steps
There are some additional steps required, but it's almost finished.

## FireWire
In order to enable communications with the device, we need to set read and write permission's on the device to everyone. You
can check what is your device using **dmesg**. 

First, connect the device on the PC and wait a few moments. Then, run the following command:
```sh
$ dmesg | grep firewire
```
On my PC, I got the following output:
```sh
[    0.600300] FireWire_ohci 0000:05:00.0: enabling device (0000 -> 0003)
[    0.666343] FireWire_ohci 0000:05:00.0: added OHCI v1.10 device as card 0, 4 IR + 8 IT contexts, quirks 0x11
[    1.166421] FireWire_core 0000:05:00.0: created device fw0: GUID 0011066600000009, S400
[ 1243.251929] FireWire_core 0000:05:00.0: phy config: new root=ffc1, gap_count=5
[ 1249.803286] FireWire_core 0000:05:00.0: phy config: new root=ffc1, gap_count=5
[ 1252.790785] FireWire_core 0000:05:00.0: created device fw1: GUID 000b990080df6000, S400
```
The last device created was fw1, then that's my Phantom Omni. From now on, we just need to change its permissions:
```sh
$ sudo chmod a+rw /dev/fw1
```
If you're not sure which fw* is your Omni or doesn't bother setting all FireWire devices permission's, just run:
```sh 
$ sudo chmod a+rw /dev/fw*
```
This should be done everytime you reconnect the FireWire device.

## Ethernet
To use the Ethernet device, you must create a new Ethernet connection and set its IPV4 connection method to link-local only. 
Second, you need to generate your config files for GTDD.

```sh 
$ cd /opt/geomagic_touch_device_driver
$ ./Geomagic_Touch_Setup 
```
A GUI appears and you should select the Geomagic Touch as the device model.
Then, click the "Add..." button and give your device the following name: Phantom Omni. Just like that, starting with Phantom, separated with a blank space and 
finishing with Omni. If the name is different, the ROS Driver won't be able to 
find the device. Next, you have to pair your device, selecting available devices
and clicking the " Pairing" button first on the GUI, then on the device itself. 
If your device is not listed here, check the device communication running 
/opt/geomagic_touch_device_driver/Geomagic_Touch_Diagnostic . Most of the times, this happens because the IPV4 isn't
a link-local only connection. 


# Troubleshooting
## FireWire
 - **[ERROR] [timestamp]: No firewire devices found.** -- Check the read and write permissions on /dev/fw*. Try reconnecting the device and running dmesg to see if connection was successful.

## Ethernet
 - **[ERROR] [timestamp]: Failed to initialize haptic device** -- Check the config files on "/opt/geomagic_touch_device_driver/config". Please make sure the device was properly configured as explained on above. Please note that any name other than "Phantom Omni" will not work.
 - **[ERROR] [geomagic library]: Fail to find libHD** --  Check the config file "/etc/profile.d/geomagic.sh".  Commment out "#export LD_LIBRARY_PATH=/opt/geomagic_touch_device_driver/lib".

# License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).
